<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="author" content="M. Safari">
		<meta name="description" content="Benchmark visualisation">
		<title>Benchmark visualisation of {{ system }}</title>
		<link rel="icon" href="https://milligram.github.io/images/icon.png">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic"/>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.css"/>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/milligram/1.4.1/milligram.css"/>
   		<script src="https://unpkg.com/gridjs/dist/gridjs.umd.js"></script>
    		<link rel="stylesheet" href="https://unpkg.com/gridjs/dist/theme/mermaid.min.css" />
   		<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.7.0/chart.min.js"></script>
    <style>
        /* Adjust the height of the chart and table container */
        #myChart, #table-container {
            height: 170px; /* Adjust this value as needed */
        }
        /* Make both columns the same width */
        .col-md-6 {
            width: 70%;
        }
    </style>
</head>
<body>
    <div class="container">
         <!-- Add a header -->
        <h3>Benchmark has been performed on {{ platform }}.</h3>
           <!-- Add a link to the input file -->
             <a href="{{ path_of_input }}" target="_blank"> View Input File</a>
        <div class="row">
            <div class="col-md-6">
                <canvas id="myChart"></canvas>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div id="table-container" class="mt-4"></div>
            </div>
        </div>
    </div>


<script>
   //# Some nice colors from https://sashat.me/2017/01/11/list-of-20-simple-distinct-colors/
    //colors = ((230, 25, 75), (60, 180, 75), (255, 225, 25), (0, 130, 200), (245, 130, 48), (145, 30, 180), (70, 240, 240), (240, 50, 230), (210, 245, 60), (250, 190, 190), (0, 128, 128), (230, 190, 255), (170, 110, 40), (255, 250, 200), (128, 0, 0), (170, 255, 195), (128, 128, 0), (255, 215, 180))
    // Use triple curly braces to prevent Jinja from escaping characters
    const jsonData = {{ dataframe | load_csv_data | tojson | safe }};
    const nodes = jsonData['{{ Lx_axis }}'].map(String);
    const data1 = jsonData['{{ Lcolumn_name }}'];
    
{% for column in table_columns %}
    const tableD{{ loop.index0 }} = jsonData[{{ column | tojson | safe }}];
{% endfor %}

    const ctx = document.getElementById('myChart').getContext('2d');
    {% if Lcomponent == 'empty' %}
        new Chart(ctx, {
            type: 'bar',
            data: {
                labels: nodes,
                datasets: [
                    {
                        label: '{{ Lcolumn_name }}',
                        data: data1,
                        backgroundColor: 'rgba(230, 25, 75, 0.7)',
                    },
                ],
            },
            options: {
                scales: {
                    x: {
                        stacked: true,
            title: {
                display: true,
                text: '{{ Lx_axis }}', // Add your Y-axis label here
                font: {
                    size: 18, // Set the font size for X-axis label
                },
            },
            ticks: {
                font: {
                    size: 16, // Set the font size for X-axis tick labels
                },
            },
                    },
                y: {
                    stacked: true,
            title: {
                display: true,
                text: 'time ({{ Ltime_unit }})', // Add your Y-axis label here
                font: {
                    size: 18, // Set the font size for X-axis label
                },
            },
            ticks: {
                font: {
                    size: 16, // Set the font size for X-axis tick labels
                },
            },
                }
            },
         },
                 plugins: [{
                id: 'custom-text-plugin',
                afterDraw: (chart) => {
                    // Add text inside the chart container
                    const text = ' {{ system }} structure: \n Evolving {{ Lcolumn_name }} ';
                    const x = chart.width / 2;
                    const y = chart.height * 1/9;
// Draw a transparent container
ctx.fillStyle = 'rgba(222, 222, 222, 0.5)'; // Set the fill color with transparency
const containerWidth = 400; // Adjust the width of the container
const containerHeight = 25; // Adjust the height of the container
ctx.fillRect(x - containerWidth / 2, y - containerHeight / 2, containerWidth, containerHeight);
                    ctx.fillStyle = '#3C4142'; // Set the text color
                    ctx.font = '20px Arial'; // Set the font size and style
                    ctx.textAlign = 'center';
                    ctx.fillText(text, x, y);
                },
            }],
        });

	
    {% else %}
     const data2 = {{ component | to_list | tojson | safe }};
     const data3 = {{ other_comp | to_list(format_numbers=True) | tojson | safe }};

        new Chart(ctx, {
            type: 'bar',
            data: {
                labels: nodes,
                datasets: [
                    {
                        label: '{{ Lcomponent }}',
                        
                        data: data2,
                        backgroundColor: 'rgba(60, 180, 75, 0.7)',
                    },
                   {
                        label: 'other components in {{ Lcolumn_name }}',
                        data: data3,
                        backgroundColor: 'rgba(145, 30, 180, 0.7)',
                    },
                ],
            },
options: {
    scales: {
        x: {
            stacked: true,
            title: {
                display: true,
                text: '{{ Lx_axis }}', // Add your X-axis label here
                font: {
                    size: 18, // Set the font size for X-axis label
                },
            },
            ticks: {
                font: {
                    size: 16, // Set the font size for X-axis tick labels
                },
            },
        },
        y: {
            stacked: true,
            title: {
                display: true,
                text: 'time ({{ Ltime_unit }})', // Add your Y-axis label here
                font: {
                    size: 18, // Set the font size for X-axis label
                },
            },
            ticks: {
                font: {
                    size: 16, // Set the font size for X-axis tick labels
                },
            },
        },
    },
},
                 plugins: [{
                id: 'custom-text-plugin',
                afterDraw: (chart) => {
                    // Add text inside the chart container
                    const text = '{{ system }} structure: Evolving component "{{ Lcomponent }}" of {{ Lcolumn_name }} ({{ plateform }})';
                    const x = chart.width / 2;
                    const y = chart.height * 1/9;
// Draw a transparent container
ctx.fillStyle = 'rgba(222, 222, 222, 0.5)'; // Set the fill color with transparency
const containerWidth = 600; // Adjust the width of the container
const containerHeight = 25; // Adjust the height of the container
ctx.fillRect(x - containerWidth / 2, y - containerHeight / 2, containerWidth, containerHeight);
                    ctx.fillStyle = '#3C4142'; // Set the text color
                    ctx.font = '20px Arial'; // Set the font size and style
                    ctx.textAlign = 'center';
                    ctx.fillText(text, x, y);
                },
            }],
        });
     
    {% endif %}
    
const tableData = [];
for (let i = 0; i < nodes.length; i++) {
    const row = [];

    {% for column in table_columns %}
        row.push(tableD{{ loop.index0 }}[i]);
    {% endfor %}

    tableData.push(row);
}

const columnNames = [{% for column in table_columns %}"{{ column }}", {% endfor %}];

new gridjs.Grid({
    columns: columnNames,
    data: tableData
}).render(document.getElementById("table-container"));


    
</script>
</body>
</html>

