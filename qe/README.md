### QuantumESPRESSO workloads

| name   | CODE  | pseudo    | electrons | nbands | FFT dimension | G-vectors | k-points | irrep  |
| ------ | ----- | ------    | --------- | ------ | ------------- | --------- | -------- | ------ |
| ausurf | PWSCF | US + cc   | 1232      | 800    | 125,  64, 200 | 763307    | 2	 | -      |
| si16l  | PH    | NC + cc   | 256	 | 154    | 80,   80, 256 | 665603    | 128 (ph) | 192    | 

