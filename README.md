# Benchmarks of MaX applications

This folder contains benchmark results of MaX3 applications.

Benchmarks are organized with the following structure

`<application>/<workload>/<platform>/<version>_<year>-<month>`

The benchmark folder contains

* result.dat : benchmark results for selected application clocks ;
* inputfiles : the folder with input for benchmark workload ;
* logfiles   : the JUBE output with the application logfiles and xml scripts.

The JUBE output has the following stucture

```
000000 # main directory
|
├── 000001_submit # workpackage directory
│   ├── done     
│   └── work
│       ├── < benchmark_inputs >
│       ├── < benchmark_outputs >
│       ├── < slurm logfiles >
│       ├── stderr # jube stderr
│       ├── stdout # jube stdout
│       └── submit.job # jobscript for submission
├── analyse.log # logfiles for analyse step (jube )
├── analyse.xml # output of analysis step (jube )
├── configuration.xml # configuration file for the benchmark
├── continue.log # logfiles for continue command (jube )
├── parse.log # logfile for parsing step (jube )
├── result # folder with result table
│   └── result.dat
├── result.log # logfile for result step (jube)
├── run.log # logfile for run step (jube )
├── timestamps
└── workpackages.xml # concretization of the different workpackages (jube )
```
